package com.friendlyarm.AndroidSDK;

import android.util.Log;

/**
 * 友善之臂技术支持邮箱：13719442657@163.com
 */
/**
 * fd: 由 设备 返回的文件描述符(用于识别设备) pos: 指定数据在 EEPROM 的位置 (0~255) byteData: 要写入的数据
 * 
 * @author Administrator
 * 
 */
public class HardwareControler {
	/* File */
	static public native int open(String devName, int flags);

	static public native int write(int fd, byte[] data);

	static public native int read(int fd, byte[] buf, int len);

	static public native int select(int fd, int sec, int usec);

	static public native void close(int fd);

	static public native int ioctlWithIntValue(int fd, int cmd, int value);

	/* *
	 * Serial Port 串口通信的接口说明 openSerialPort打开串口通信
	 */
	static public native int openSerialPort(String devName, long baud,
			int dataBits, int stopBits);

	static public native int openSerialPortEx(String devName, long baud,
			int dataBits, int stopBits, String parityBit, String flowCtrl);

	/**
	 * 开关LED的接口说明 ledID:指定要开关哪一个LED（取值0~3） ledState:1表示亮；0表示灭 返回值说明：成功返回0，失败返回-1
	 */
	static public native int setLedState(int ledID, int ledState);

	/* 让PWM蜂鸣器发声和停止发声的接口说明 */
	static public native int PWMPlay(int frequency);

	static public native int PWMStop();

	/**
	 * num: The channel that should be updated with the new values (0..15) on:
	 * The tick (between 0..4095) when the signal should transition from low to
	 * high off:the tick (between 0..4095) when the signal should transition
	 * from high to low
	 * 
	 * @param num
	 * @param on
	 * @param off
	 *
	 */
	static public void setPWM(byte num, byte on, byte off) {
		int fd = openI2CDevice();
		int pos = 0x00;
		// byte[] byteData = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
		// 15 };
		writeByteToI2C(fd, pos, num, 1);
		writeByteToI2C(fd, pos, on, 1);
		writeByteToI2C(fd, pos, (byte) (on >> 8), 1);
		writeByteToI2C(fd, pos, off, 1);
		writeByteToI2C(fd, pos, (byte) (off >> 8), 1);

	}

	/* ADC（Analog to Digital Converter）读取模拟数字转换器的接口的说明 */
	static public native int readADC();

	static public native int readADCWithChannel(int channel);

	static public native int[] readADCWithChannels(int[] channels);

	/* I2C */
	/**
	 * 参数说明：fd: I2C 设备的文件描述符 slave: I2C 设备地址， 例如 EEPROM 设备一般是 0x50 返回值说明：成功返回
	 * 0，失败返回-1
	 * 
	 * @param fd
	 * @param slave
	 * @return
	 */
	// 设置要操作的 I2C 设备地址，例如EEPROM设备一般是0x50
	static public native int setI2CSlave(int fd, int slave);

	/*
	 * 参数说明： fd: I2C 设备的文件描述符 timeout: 超时时间 返回值说明： 成功返回 0，失败返回-1
	 */
	// 设置超时时间 (ioctlI2C_TIMEOUT)
	static public native int setI2CTimeout(int fd, int timeout);

	/*
	 * fd: I2C 设备的文件描述符 retries: 重试次数 返回值说明： 成功返回 0，失败返回-1
	 */
	// 设置重试次数 (ioctlI2C_RETRIES)
	static public native int setI2CRetries(int fd, int retries);

	static public native int writeByteToI2C(int fd, int pos, byte byteData,
			int wait_ms);

	/*
	 * 参数说明： fd: I2C 设备的文件描述符 pos: 字节位置 byteData:要写入的数据 wait_ms: 等待指定的时间(毫秒)
	 * 返回值说明： 成功返回 0，失败返回-1
	 */
	// 写一个字节的数据到 I2C 设备的指定位置,并等待指定的时间(毫秒)
	static public native int I2CWritreByteTo(int fd, int pos, char byteData,
			int wait_ms);

	static public native int readByteFromI2C(int fd, int pos, int wait_ms);

	/*
	 * 参数说明： fd: I2C 设备的文件描述符 pos: 字节位置 wait_ms: 等待指定的时间(毫秒) 返回值说明： 成功返回
	 * 0，失败返回-1
	 */
	// 从 I2C 设备指定的位置读一个字节的数据，并等待指定的时间(毫秒)
	static public native int I2CReadByteFrom(int fd, int pos, int wait_ms);

	/* SPI */
	static public native int setSPIWriteBitsPerWord(int spi_fd, int bits);

	static public native int setSPIReadBitsPerWord(int spi_fd, int bits);

	static public native int setSPIBitOrder(int spi_fd, int order);

	static public native int setSPIClockDivider(int spi_fd, int divider);

	static public native int setSPIDataMode(int spi_fd, int mode);

	static public native int SPItransferOneByte(int spi_fd, byte byteData,
			int spi_delay, int spi_speed, int spi_bits);

	static public native int SPItransferBytes(int spi_fd, byte[] writeData,
			byte[] readBuff, int spi_delay, int spi_speed, int spi_bits);

	static public native int writeBytesToSPI(int spi_fd, byte[] writeData,
			int spi_delay, int spi_speed, int spi_bits);

	static public native int readBytesFromSPI(int spi_fd, byte[] readBuff,
			int spi_delay, int spi_speed, int spi_bits);

	/* GPIO */
	/**
	 * int exportGPIOPin(int pin) 
	 * 参数说明： pin: GPIO 引脚编号 
	 * 返回值说明： 成功返回 0，失败返回负数
	 * 功能说明：通知系统需要导出控制的 GPIO 引脚编号， 相当于执行命令 echo pin > /sys/class/gpio/export
	 */
	static public native int exportGPIOPin(int pin);

	/**
	 * int unexportGPIOPin(int pin) 
	 * 参数说明： pin: GPIO 引脚编号 
	 * 返回值说明： 成功返回 0，失败返回负数
	 * 功能说明：通知系统取消导出某个GPIO引脚， 相当于执行命令 echo pin > /sys/class/gpio/unexport
	 */
	static public native int unexportGPIOPin(int pin);

	// GPIOEnum.LOW or GPIOEnum.HIGH
	/**
	 * int setGPIOValue(int pin, int value) 
	 * 参数说明： pin: GPIO 引脚编号 value: 传入GPIOEnum.LOW 表示输出低电 平，传入 GPIOEnum.HIGH 表示输出高电平 
	 * 返回值说明： 成功返回 0，失败返回负数
	 * 功能说明：对某个引脚输出高或低电平
	 */
	static public native int setGPIOValue(int pin, int value);

	/**
	 * int getGPIOValue(int pin) 
	 * 参数说明：pin: GPIO 引脚编号
	 * 返回值说明：成功返回 GPIOEnum.LOW 表示输出低电平，返回 GPIOEnum.HIGH 表示输出高电平，失败返回负数
	 * 功能说明：查询某个引脚的状态（高或低电平）
	 */
	static public native int getGPIOValue(int pin);

	// GPIOEnum.IN or GPIOEnum.OUT
	/**
	 * int setGPIODirection(intpin, int direction)
	 * 参数说明：pin: GPIO 引脚编号   
	 *		direction: 传入 GPIOEnum.IN 表示输入，GPIOEnum.OUT 表示输出
	 * 返回值说明：成功返回 0，失败返回负数
	 * 功能说明：配置引脚功能为输出或者输入
	 */
	static public native int setGPIODirection(int pin, int direction);
    /**
     * int getGPIODirection(int pin) 
     * 参数说明： pin: GPIO 引脚编号
     * 成功返回 GPIOEnum.IN 表示输入，返回GPIOEnum.OUT 表示输出，失败返回负数
     * 功能说明：查询引脚功能 (为输出或者输入)
     */
	static public native int getGPIODirection(int pin);

	/******** EEPROM数据的写入与读取的接口说明 *****************/
	/*
	 * 接口的使用说明： 先通过调用 openI2CDevice 打开 IIC 设备，然后需要创建一个新线程，在线程中调用
	 * writeByteDataToI2C 将数据写入 EEPROM，或者调用 readByteDataFromI2C 从 EEPROM 读出数
	 * 据，为什么要创建新线程呢？因为 writeByteDataToI2C 和 readByteDataFromI2C 函数在读写之后 都会延时 10
	 * 毫秒左右，如果在 GUI 线程中调用会导致界面短暂阻塞。 EEPROM 可存储 256 个字节的数据，所以在读写时需要指定的位置范围是
	 * 0~255，每次只 能读写一个字节。 EEPROM 操作完毕后，需要调用 close 关闭文件描述符。
	 */
	/* *
	 * OldInterface: for EEPROM EEPROM数据的写入与读取的接口说明 EEPROM (Electrically
	 * Erasable Programmable Read-Only Memory)， 带电可擦可编程只读存储器--一种掉电后数据不丢失的存储芯片。
	 * EEPROM 可以在电脑上或专用设备上擦除已有信息，重新编程。一般用在即插即用。
	 */
	static public native int openI2CDevice();// 打来IIC设备

	/**
	 * 参数说明： fd: 由 openI2CDevice 返回 的文件描述符 pos: 指定数据在 EEPROM 的 位置 (0~255)
	 * byteData: 要写入的数据 返回值说明： 成功返回写入的字节数，出错 返回-1。
	 * 
	 * 功能说明： 往 EEPROM 中写入数据 (每次只能写一个 byte)。 注意，该函数是个耗时的函数(约 10 毫秒)，需要在工作线程中调用它。
	 */
	static public native int writeByteDataToI2C(int fd, int pos, byte byteData);

	/**
	 * 参数说明： fd: 由 openI2CDevice 返回 的文件描述符 pos: 指定数据在 EEPROM 的 位置 (0~255) 返回值说明：
	 * 成功返回读取的数据（可强， 出错返回-1，如果在调 read 之 前已到达文件末尾，则这次 read 返回 0。 返回值的类型是 int，你需要转
	 * 换成 byte。
	 * 
	 * 功能说明：从打开的设备或文件中读取数据。注意，该函数是个耗时的函数(约 10 毫秒)，需要在工作线程中调用它。
	 */
	static public native int readByteDataFromI2C(int fd, int pos);

	/******** EEPROM数据的写入与读取的接口说明 *****************/

	/* return 6410 or 210 or 4412 */
	static public native int getBoardType();

	static {
		try {
			System.loadLibrary("friendlyarm-hardware");
		} catch (UnsatisfiedLinkError e) {
			Log.d("HardwareControler",
					"libfriendlyarm-hardware library not found!");
		}
	}
}
