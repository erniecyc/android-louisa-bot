package com.friendlyarm.AndroidSDK;


import android.content.Context;
import android.util.Log;
import com.aitech.hardwareconctrl.demo.MainActivity;
import com.friendlyarm.AndroidSDK.EngineParameter;
import com.friendlyarm.AndroidSDK.HardwareControler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;

public class SomeUtils {
    private static final String TAG = "airobot";
    public static Timer timer;// 创建舵机定时器对象
    public static TimerTask mTimerTask;

    public static Timer smiletimer;// 创建舵机定时器对象
    public static TimerTask mSmileTimerTask;
    public static Timer kisstimer;// 创建舵机定时器对象
    public static TimerTask mKissTimerTask;
    public static Timer openmouthtimer;// 创建舵机定时器对象
    public static TimerTask mopenmouthTimerTask;
    public static Timer sadnesstimer;// 创建舵机定时器对象
    public static TimerTask mSadnessTimerTask;
    public static Timer clowntimer;// 创建舵机定时器对象
    public static TimerTask mClownTimerTask;
    public static String devName = "/dev/ttyAMA3";//打开16路舵机控制板的串口 ttyAMA3
    public  static int  devfd = -1;
    public  static int  devfd4 = -1;
    public  static int  devfd3 = -1;
    public static String devName4 = "/dev/ttyAMA4"; ///dev/ttyAMA4";
    public static int speed4 = 38400;//baud波特率参数
    public static int speed = 9600;//baud波特率参数 //9600
    public static int dataBits = 8;
    public static int stopBits = 1;
    public static Boolean mouthFlag;
    /**
     * Serial Port 串口通信的接口说明 openSerialPort打开串口通信 初始化串口通信接口
     */
    public static void initSerialPort() {

        devfd = HardwareControler.openSerialPort(devName,
                speed, dataBits, stopBits);
        Log.e(TAG, "---- 舵机控制串口打开成功3 Constants.devfd3  "+ devfd3  );
        if (devfd >= 0) {// 表示初始化串口成功，打开串口成功
            Log.e(TAG, "---- 舵机控制串口打开成功2 Constants.devfd  "+devfd );
            /*********** 启动程序的时候所有动作初始位置 **************/
            // 控制嘴闭合
            writeTo(EngineParameter.data00);// 速度
            writeTo(EngineParameter.data04);// 正常
            // 控制 眼睛左眼皮正常
            writeTo(EngineParameter.data20);// 速度
            writeTo(EngineParameter.data25);// 睁开123
            // 控制 眼睛右眼皮正常
            writeTo(EngineParameter.data40);// 速度
            writeTo(EngineParameter.data45);// 睁开323
            // 控制 眼睛两个眼珠摆正
            writeTo(EngineParameter.data60);// 速度
            writeTo(EngineParameter.data64);// 正位522
            // 控制 脖子正位
            writeTo(EngineParameter.data80);// 速度
            writeTo(EngineParameter.data88);// 正位72
            // 控制 脸部左侧正位
            writeTo(EngineParameter.data120);// 速度
            writeTo(EngineParameter.data1208);// 90度
            // 控制 脸部右侧正位
            writeTo(EngineParameter.data130);// 速度
            writeTo(EngineParameter.data1308);// 90度
            // 控制 点头正位
            writeTo(EngineParameter.data140);// 速度
            writeTo(EngineParameter.data144);// 正位
        } else {
            devfd = -1;
            // showTip("串口打开失败");
            Log.e(TAG, "---- 舵机控制串口打开失败");
        }
        devfd4 = HardwareControler.openSerialPort(devName4,
                speed4, dataBits, stopBits);
        if (devfd4 >= 0) {
            Log.e(TAG, "---- 舵机控制串口 devfd4 打开成功");
        }else{
            Log.e(TAG, "---- 舵机控制串口 devfd4 打开失败");
        }
    }

    public static Boolean opensuss(){
        if(devfd >= 0){
            return  true;
        }else{
            return  false;
        }
    }

    public static void startTimer(){
        if (timer == null) {
            timer = new Timer();
        }
        if (mTimerTask == null) {
            mTimerTask = new TimerTask() {
                int count = 0;
                int count1 = 0;
                boolean cycleEYEFlag = true;
                //boolean neckFlag = true;
                @Override
                public void run() {
                    count++;
                    count1++;
                    if (count == 1) {
                        // 控制 眼睛左眼皮闭合
                        writeTo(EngineParameter.data21);// 速度
                        writeTo(EngineParameter.data26);// 闭合
                        // 控制 眼睛右眼皮闭合
                        writeTo(EngineParameter.data41);// 速度
                        writeTo(EngineParameter.data46);// 闭合
                        Log.e(TAG, "----1闭眼睛" + count);
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // 控制 眼睛左眼皮睁开
                        writeTo(EngineParameter.data21);// 速度
                        writeTo(EngineParameter.data25);// 睁开
                        // 控制 眼睛右眼皮睁开
                        writeTo(EngineParameter.data41);// 速度
                        writeTo(EngineParameter.data45);// 睁开
                        Log.e(TAG, "----2睁眼睛" + count);
                    } else if (count == 2) {
                        if (cycleEYEFlag) {
                            // 控制 眼睛两个眼珠左转
                            writeTo(EngineParameter.data60);// 速度
                            writeTo(EngineParameter.data66);// 左转data523
                            Log.e(TAG, "----3眼珠左转" + count);
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            // 控制 眼睛两个眼珠正位
                            writeTo(EngineParameter.data60);// 速度
                            writeTo(EngineParameter.data64);// 正位522
                            cycleEYEFlag = false;
                            Log.e(TAG, "----4眼珠正位" + count);
                        }
                    }else if(count == 3) {
                        if (!cycleEYEFlag) {
                            // 控制 眼睛两个眼珠右转
                            writeTo(EngineParameter.data60);// 速度
                            writeTo(EngineParameter.data65);// 右转
                            Log.e(TAG, "----5眼珠右转" + count);
                            // 控制 脸部左侧左转
                            writeTo(EngineParameter.data120);// 速度
                            writeTo(EngineParameter.data129);// 左转9°
                            // 控制脸部右侧右转
                            writeTo(EngineParameter.data130);// 速度
                            writeTo(EngineParameter.data139);// 右转9°
                            Log.e(TAG, "----6脸部左右上拉" + count);
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            // 控制 眼睛两个眼珠正位
                            writeTo(EngineParameter.data60);// 速度
                            writeTo(EngineParameter.data64);// 正位
                            cycleEYEFlag = true;
                            Log.e(TAG, "----7眼珠正位" + count);
                            // 控制 脸部左侧正位
                            writeTo(EngineParameter.data120);// 速度
                            writeTo(EngineParameter.data1208);// 正位
                            // 控制 脸部右侧正位
                            writeTo(EngineParameter.data130);// 速度
                            writeTo(EngineParameter.data1308);// 正位
                            Log.e(TAG, "----8脸部左右回正" + count);
                        }
                    }
                    else if (count == 4) {
                        count = 0;
                    }
                }
            };
            timer.schedule(mTimerTask, 2000, 1500);
        }
    }

    public static void speak(){
        stopTimer();
        startopenmouthTimer();
    }
    public static void stopspeak(){
        stopTimer();
        destroyopenmouthTimer();
    }

    public static void makeface(){
        SomeUtils.stopTimer();
        SomeUtils.startClownTimer();
    }
    public static void makesadness(){
        stopTimer();
        startSadnessTimer();
    }
    public static void smile(){
        stopTimer();
        startSmileTimer();
    }

    public static void kiss(){
        stopTimer();
        startKissTimer();
    }
    public static void eyesrotate(){
        stopTimer();
        startTimer();
    }
    public static void writCommonDate(int location,int angle,int speed){
        stopTimer();
        int locations=0x00;
        int mSpeed=0x05;
        if(location==1){
            //一号舵机 左眼皮
            locations=0x02;
            if(angle<90||angle>126){
                angle=126;
            }
        }else if(location==2){
            //二号舵机 右眼皮
            locations=0x04;
            if(angle<54||angle>90){
                angle=54;
            }
        }else if(location==3){
            //三号舵机 眼睛
            locations=0x06;
            //72 90 108
            if(angle<72||angle>108){
                angle=90;
            }
        }else if(location==4){
            //四号舵机 左脸舵机
            locations=0x0c;
            if(angle<58||angle>140){
                angle=90;
            }
        }else if(location==5){
            //五号舵机 右脸舵机
            locations=0x0d;
            if(angle<45||angle>120){
                angle=90;
            }
        }else if(location==6){
            //六号舵机 嘴巴
            locations=0x00;
            if(angle<85||angle>153){
                angle=85;
            }
        } else if(location==7){
            //七号舵机 脖子
            locations=0x08;
            if(angle<63||angle>117){
                angle=90;
            }
        }
        else{
            locations=0x02;
            angle=126;
        }

        if(speed==5){
            mSpeed=0x05;
        }else if(speed==10){
            mSpeed=0x10;
        }else if(speed==15){
            mSpeed=0x14;
        }else{
            mSpeed=0x05;
        }
        int tempangle=angle*11+500;
        Log.e(TAG, "----xjy 20190621 writCommonDate:" + tempangle+" angle:"+angle+" locations: "+locations);
        int[] angles={tempangle};
        byte[] intToBytes=intToBytes(angles,0);
        byte[] speedS = {(byte) 0xFF, 0x01, (byte) locations, (byte) mSpeed, 0x00};
        byte[] date = { (byte) 0xFF, 0x02, (byte) locations,intToBytes[0],intToBytes[1]};
        SomeUtils.writeTo(speedS);
        SomeUtils.writeTo(date);
        //    return    date;
    }

    public static void Openmouth() {
        // SomeUtils.stopTimer();
        stopTimer();
        // 控制 眼睛左眼皮闭合
        Log.e(TAG, "---- xjy 20190621  张嘴 1");
        SomeUtils.writeTo(EngineParameter.data03);// 速度
        SomeUtils.writeTo(EngineParameter.data031);// 闭合 原始数据//data26
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "---- xjy 20190621  张嘴 2");
        SomeUtils.writeTo(EngineParameter.data03);// 速度
        SomeUtils.writeTo(EngineParameter.data09);//
    }


    public static byte[] intToBytes(int[] src,int offset)
    {
        byte[] values = new byte[src.length*4];
        for(int i = 0; i < src.length; i++ )
        {
            values[offset+3] = (byte)((src[i] >> 24) & 0xFF);
            values[offset+2] = (byte)((src[i] >> 16) & 0xFF);
            values[offset+1] = (byte)((src[i]  >> 8) & 0xFF);
            values[offset] = (byte)(src[i]  & 0xFF);
            offset+=4;
        }
        return values;
    }

    public static void blinkeyes(){
        stopTimer();
        Log.e(TAG, "----   眨眼睛 ");
        writeTo(EngineParameter.data21);// 速度
        writeTo(EngineParameter.data26);// 闭合
        // 控制 眼睛右眼皮闭合
        writeTo(EngineParameter.data41);// 速度
        writeTo(EngineParameter.data46);// 闭合
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 控制 眼睛左眼皮睁开
        writeTo(EngineParameter.data21);// 速度
        writeTo(EngineParameter.data25);// 睁开
        // 控制 眼睛右眼皮睁开
        writeTo(EngineParameter.data41);// 速度
        writeTo(EngineParameter.data45);// 睁开
    }

    public static void blinklefteyes(){
        // 控制 眼睛左眼皮闭合
        stopTimer();
        Log.e(TAG, "---- xjy 20190621 眨左眼 ");
        writeTo(EngineParameter.data21);// 速度
        writeTo(EngineParameter.data26);// 闭合 90
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 控制 眼睛左眼皮睁开
        writeTo(EngineParameter.data21);// 速度
        writeTo(EngineParameter.data25);// 睁开

    }

    public static void blinkrighteyes(){
        stopTimer();
        Log.e(TAG, "---- xjy 20190621  眨右眼 ");
        writeTo(EngineParameter.data41);// 速度
        writeTo(EngineParameter.data46);// 闭合
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 控制 眼睛右眼皮睁开
        writeTo(EngineParameter.data41);// 速度
        writeTo(EngineParameter.data45);// 睁开
    }

    public static void turnNeck(){
        stopTimer();
        writeTo(EngineParameter.data80);// 速度
        writeTo(EngineParameter.data811);// 左转18°
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeTo(EngineParameter.data80);// 速度
        writeTo(EngineParameter.data88);// 正位72
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeTo(EngineParameter.data80);// 速度
        writeTo(EngineParameter.data85);// 右转18°

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeTo(EngineParameter.data80);// 速度
        writeTo(EngineParameter.data88);// 正位72
    }


    public static void stopTimer() {

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask = null;
        }
    }
    private static synchronized void destroySmileTimer() {
        Log.e(TAG, "---- destroyMyTimer : smiletimer = " + smiletimer + "   mSmileTimerTask=" + mSmileTimerTask);
        if (mSmileTimerTask != null) {
            mSmileTimerTask.cancel();
            mSmileTimerTask = null;
        }
        if (smiletimer != null) {
            smiletimer.cancel();
            smiletimer = null;
        }
    }

    public static void startSmileTimer() {
        destroySmileTimer();
        initSmileTimer();
        Log.e(TAG, "---- startSmileTimer : smiletimer = "+ smiletimer +"   mSmileTimerTask=" + mSmileTimerTask);
        if((smiletimer != null)&&(mSmileTimerTask != null)){
            smiletimer.schedule(mSmileTimerTask, 100, 1500);
        }
    }
    public static void initSmileTimer() {


        if (smiletimer == null) {
            smiletimer = new Timer();
        }
        if (mSmileTimerTask == null) {
            mSmileTimerTask = new TimerTask() {
                int count = 0;
                int count1 = 0;
                Boolean step1;
                @Override
                public void run() {
                    count++ ;
                    Log.e(TAG, "----initSmileTimer ：count =" + count + "  count1 = " + count1);
                    if(count1 == 1 ){  // 2
                        count = 0;
                        count1 = 0;
                        writeTo(EngineParameter.data120); //
                        writeTo(EngineParameter.data1208);//
                        writeTo(EngineParameter.data130); //
                        writeTo(EngineParameter.data1308);//

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data25);// 左眼睁大
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data45);// 右眼闭合
                        destroySmileTimer();
                        // startTimer();
                        return;
                    }
                    if(count == 1){
                        Log.e(TAG, "----count == 1: 角度 =" + 135 +  "速度"+ 10);

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data24);// 左眼睁大
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data44);// 右眼闭合

                        writeTo(EngineParameter.data121);// 速度 5
                        writeTo(EngineParameter.data1214);
                        writeTo(EngineParameter.data131 );// 速度 5
                        writeTo(EngineParameter.data1302);

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data26);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data46);// 右眼闭合
                    }
                    if(count == 3) {
                        Log.e(TAG, "----count == 3: 角度 =" + 90+ "速度"+ 10);
                        writeTo(EngineParameter.data120); //
                        writeTo(EngineParameter.data1208);//
                        writeTo(EngineParameter.data130); //
                        writeTo(EngineParameter.data1308);//

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data26);// 左眼正常
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data46);// 右眼正常
                    }
                    if(count == 4){
                        Log.e(TAG, "----count == 4: 角度 =" + 135 + "速度"+ 10);
                        writeTo(EngineParameter.data121 );// 速度
                        writeTo(EngineParameter.data1214 );// 角度 135°
                        writeTo(EngineParameter.data131);// 速度
                        writeTo(EngineParameter.data1302);// 角度 45°

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data26);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data46);// 右眼闭合
                        count = 0;
                        count1 ++;
                    }
                }
            };
        }
    }
    private static synchronized void destroyClownTimer() {
        Log.e(TAG, "---- destroyClownTimer : clowntimer = " + clowntimer + "   mClownTimerTask=" + mClownTimerTask);
        if (mClownTimerTask != null) {
            mClownTimerTask.cancel();
            mClownTimerTask = null;
        }
        if (clowntimer != null) {
            clowntimer.cancel();
            clowntimer = null;
        }
    }
    public static void startClownTimer() {
        destroyClownTimer();
        initClownTimer();
        Log.e(TAG, "---- startClownTimer : clowntimer = "+ clowntimer +"   mClownTimerTask=" + mClownTimerTask);
        if((clowntimer != null)&&(mClownTimerTask != null)){
            clowntimer.schedule(mClownTimerTask, 100, 1500);
        }
    }
    public static void initClownTimer() {

        if (clowntimer == null) {
            clowntimer = new Timer();
        }
        if (mClownTimerTask == null) {
            mClownTimerTask = new TimerTask() {
                int count = 0;
                int count1 = 0;
                @Override
                public void run() {
                    count++;
                    Log.e(TAG, "----initClownTimer =" + count +"  count1 =" + count1);

                    if(count1 == 1){
                        destroyClownTimer();
                        //    startTimer();
                        count = 0;
                        count1 = 0;
                    }
                    if (count == 1) {
                        writeTo(EngineParameter.data120);// 左脸速度
                        writeTo(EngineParameter.data1212);// 左脸上拉//135
                        writeTo(EngineParameter.data130);// 右脸速度
                        writeTo(EngineParameter.data1308);// 右脸水平 90

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data26);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data44);// 右眼睁大
                    }
                    if( count == 2){
                        writeTo(EngineParameter.data120);// 左脸速度
                        writeTo(EngineParameter.data1208);// 左脸水平
                        writeTo(EngineParameter.data130);// 右脸速度
                        writeTo(EngineParameter.data1304);// 右脸上拉

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data24);// 左眼睁大
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data46);// 右眼闭合

                    }
                    if( count == 3){
                        writeTo(EngineParameter.data120);// 左脸速度
                        writeTo(EngineParameter.data1212);// 左脸上拉
                        writeTo(EngineParameter.data130);// 右脸速度
                        writeTo(EngineParameter.data1308);// 右脸水平

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data24);// 左眼睁大
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data46);// 右眼闭合
                    }
                    if( count == 4){
                        writeTo(EngineParameter.data120);// 左脸速度
                        writeTo(EngineParameter.data1208);// 左脸水平
                        writeTo(EngineParameter.data130);// 右脸速度
                        writeTo(EngineParameter.data1304);// 右脸上拉

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data26);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data44);// 右眼睁大
                    }
                    if(count == 5){
                        writeTo(EngineParameter.data120);// 左脸速度
                        writeTo(EngineParameter.data1206);// 左脸水平
                        writeTo(EngineParameter.data130);// 右脸速度
                        writeTo(EngineParameter.data1312);// 右脸上拉

                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data24);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data44);// 右眼睁大

                    }
                    if(count == 6){
                        writeTo(EngineParameter.data120);// 左脸速度
                        writeTo(EngineParameter.data1208);// 左脸水平
                        writeTo(EngineParameter.data130);// 右脸速度
                        writeTo(EngineParameter.data1308);// 右脸上拉

                        writeTo(EngineParameter.data20);// 左眼速度
                        writeTo(EngineParameter.data25);// 左眼闭合
                        writeTo(EngineParameter.data40);// 右眼速度
                        writeTo(EngineParameter.data45);// 右眼睁大

                        count = 0;
                        count1 ++;
                    }
                }
            };
        }
    }

    private static synchronized void destroySadnessTimer() {
        Log.e(TAG, "---- destroySadnessTimer : sadnesstimer = " + clowntimer + "   mSadnessTimerTask=" + mClownTimerTask);
        if (mSadnessTimerTask != null) {
            mSadnessTimerTask.cancel();
            mSadnessTimerTask = null;
        }
        if (sadnesstimer != null) {
            sadnesstimer.cancel();
            sadnesstimer = null;
        }

    }
    public static void startSadnessTimer() {
        destroySadnessTimer();
        initSadnessTimer();
        Log.e(TAG, "---- startSadnessTimer : sadnesstimer = "+ sadnesstimer +"   mSadnessTimerTask=" + mSadnessTimerTask);
        if((sadnesstimer != null)&&(mSadnessTimerTask != null)){
            sadnesstimer.schedule(mSadnessTimerTask, 100, 1500);
        }
    }
    public static void initSadnessTimer()  {

        if (sadnesstimer == null) {
            sadnesstimer = new Timer();
        }
        if (mSadnessTimerTask == null) {
            mSadnessTimerTask = new TimerTask() {
                int count = 0;
                int count1 = 0;
                @Override
                public void run() {
                    count++ ;
                    Log.e(TAG, "----startSadnessTimer count =" + count + "  count1 = "+ count1);
                    if(count1 == 2){
                        Log.e(TAG, "----startSadnessTimer  count1 =" + count1);
                        destroySadnessTimer();
                        //   startTimer();
                        count = 0;
                        count1 = 0;
                    }
                    if (count == 1) {
                        Log.e(TAG, "----startSadnessTimer count =" + count);
                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data26);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data46);// 右眼闭合

                        writeTo(EngineParameter.data120); //
                        writeTo(EngineParameter.data1208);//
                        writeTo(EngineParameter.data130); //
                        writeTo(EngineParameter.data1308);//
                    }
                    if( count == 2){
                        Log.e(TAG, "----startSadnessTimer count =" + count);
                        writeTo(EngineParameter.data121);// 速度 5
                        writeTo(EngineParameter.data1206);
                        writeTo(EngineParameter.data131 );// 速度 5
                        writeTo(EngineParameter.data1310);
                    }
                    if( count == 4){
                        Log.e(TAG, "----startSadnessTimer count =" + count);
                        writeTo(EngineParameter.data120);// 速度 5
                        writeTo(EngineParameter.data1208);
                        writeTo(EngineParameter.data130 );// 速度 5
                        writeTo(EngineParameter.data1308);
                        count1++;
                    }

                }
            };
        }
    }


    public static synchronized void destroyopenmouthTimer() {
        Log.e(TAG, "---- destroyKissTimer : openmousetimer = " + openmouthtimer + "   mopenmouseTimerTask=" + mopenmouthTimerTask);
        if (mopenmouthTimerTask != null) {
            mopenmouthTimerTask.cancel();
            mopenmouthTimerTask = null;
        }
        if (openmouthtimer != null) {
            openmouthtimer.cancel();
            openmouthtimer = null;
        }
        writeTo(EngineParameter.data03);//
        writeTo(EngineParameter.data09);//
    }

    public static void startopenmouthTimer() {
        destroyopenmouthTimer();
        initopenmouthTimer();
        mouthFlag=true;
        Log.e(TAG, "---- startKissTimer : openmousetimer = "+ openmouthtimer +"   mopenmouseTimerTask=" + mopenmouthTimerTask);
        if((openmouthtimer != null)&&(mopenmouthTimerTask != null)){
            openmouthtimer.schedule(mopenmouthTimerTask, 100, 100);
        }
    }

    public static void initopenmouthTimer(){
        if (openmouthtimer == null) {
            openmouthtimer = new Timer();
        }
        if (mopenmouthTimerTask == null) {
            mopenmouthTimerTask = new TimerTask() {
                int count = 0;
                int count1 = 0;
                @Override
                public void run() {
                    count++ ;
                    Log.e(TAG, "----initopenmouseTimer count =" + count +"  count1="+count1);
                    if(count1 == 100){
                        Log.e(TAG, "----initopenmouseTimer  count1 =" + count1);
                        destroyopenmouthTimer();
                        count = 0;
                        count1 = 0;
                    }

                    if (count % 3 == 0 && mouthFlag) {
                        // 控制嘴闭合
                        writeTo(EngineParameter.data03);// 速度
                        writeTo(EngineParameter.data09);
                        Log.e(TAG, "----  initopenmouthTimer 张嘴 count："+count+" mouseFlag:" +mouthFlag+ " count1："+count1);
                        mouthFlag = false;
                        // Log.e(TAG, "闭嘴");
                    } else if (count % 3 == 0 && !mouthFlag) {
                        // 控制嘴张开
                        writeTo(EngineParameter.data03);// 速度
                        writeTo(EngineParameter.data10);
                        Log.e(TAG, "---- initopenmouthTimer 闭嘴 count："+count+" mouseFlag:"+mouthFlag+ " count1："+count1);
                        mouthFlag = true;
                        count1++;
                    }
                }
            };
        }
    }

    private static synchronized void destroyKissTimer() {
        Log.e(TAG, "---- destroyKissTimer : kisstimer = " + kisstimer + "   mKissTimerTask=" + mKissTimerTask);
        if (mKissTimerTask != null) {
            mKissTimerTask.cancel();
            mKissTimerTask = null;
        }
        if (kisstimer != null) {
            kisstimer.cancel();
            kisstimer = null;
        }

    }
    public static void startKissTimer() {
        destroyKissTimer();
        initKissTimer();
        Log.e(TAG, "---- startKissTimer : kisstimer = "+ kisstimer +"   mKissTimerTask=" + mKissTimerTask);
        if((kisstimer != null)&&(mKissTimerTask != null)){
            kisstimer.schedule(mKissTimerTask, 100, 1500);
        }
    }
    public static void initKissTimer()  {
        if (kisstimer == null) {
            kisstimer = new Timer();
        }
        if (mKissTimerTask == null) {
            mKissTimerTask = new TimerTask() {
                int count = 0;
                int count1 = 0;
                @Override
                public void run() {
                    count++ ;
                    Log.e(TAG, "----initKissTimer count =" + count +"  count1="+count1);
                    if(count1 == 2){
                        Log.e(TAG, "----initKissTimer  count1 =" + count1);
                        destroyKissTimer();
                        //    startTimer();
                        count = 0;
                        count1 = 0;
                    }

                    if (count == 1) {
                        Log.e(TAG, "----initKissTimer count =" + count);
                        writeTo(EngineParameter.data121);// 左脸速度
                        writeTo(EngineParameter.data1208);// 左脸水平
                        writeTo(EngineParameter.data131);// 右脸速度
                        writeTo(EngineParameter.data1308);// 右脸上拉
                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data26);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data46);// 右眼睁大
                    }
                    if( count == 2){
                        Log.e(TAG, "----initKissTimer count =" + count);
                        writeTo(EngineParameter.data121);// 左脸速度
                        //writeTo(EngineParameter.data1205);//左脸水平
                        writeTo(EngineParameter.data12060);//左脸水平
                        writeTo(EngineParameter.data131); //右脸速度
                        //writeTo(EngineParameter.data1313); //右脸上拉
                        writeTo(EngineParameter.data13100); //右脸上拉
                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data24);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data44);// 右眼闭合
                    }
                    if( count == 4){
                        Log.e(TAG, "----initKissTimer count =" + count);
                        writeTo(EngineParameter.data120);// 速度 5
                        writeTo(EngineParameter.data1208);
                        writeTo(EngineParameter.data130 );// 速度 5
                        writeTo(EngineParameter.data1308);
                        writeTo(EngineParameter.data21);// 左眼速度
                        writeTo(EngineParameter.data25);// 左眼闭合
                        writeTo(EngineParameter.data41);// 右眼速度
                        writeTo(EngineParameter.data45);// 右眼睁大
                        count1++;
                    }
                }
            };
        }
    }

    public static void writeTo(byte[] data) {
        HardwareControler.write(devfd, data);// 速度10

    }

}
