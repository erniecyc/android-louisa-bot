package com.aitech.hardwareconctrl.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.friendlyarm.AndroidSDK.SomeUtils;
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public Button stopopenmouthButton;
    public Button makefaceButton;
    public Button makesadButton;
    public Button openmouthButton;
    public Button smileButton;
    public Button kissButton;
    public Button eyesrotateButton;
    public Button blinkeyesButton;
    public Button blinklefteyesButton;
    public Button blinkrighteyesButton;
    public Button turnNeckButton;
    public TextView logInfor;
    public String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logInfor= findViewById(R.id.log);
        initview();
        SomeUtils.initSerialPort();
        if(SomeUtils.opensuss()){
            logInfor.setText("/dev/ttyAMA3 串口打开成功");
        }else{
            logInfor.setText("/dev/ttyAMA3 串口打开失败");
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.speak:
                SomeUtils.speak();
                break;
            case R.id.stopspeak:
               SomeUtils.stopspeak();
                break;
            case R.id.makeface:
                SomeUtils.makeface();
                break;
            case R.id.makesadness:
                SomeUtils.makesadness();
                break;
            case R.id.smile:
                SomeUtils.smile();
                break;
            case R.id.kiss:
                SomeUtils.kiss();
                break;
            case R.id.eyesrotate:
                SomeUtils.eyesrotate();
                break;
            case R.id.blinkeyes:
                SomeUtils.blinkeyes();
                break;
            case R.id.blinklefteyes:
                SomeUtils.blinklefteyes();
                break;
            case R.id.blinkrighteyes:
                SomeUtils.blinkrighteyes();
                break;
            case R.id.turnNeck:
                SomeUtils.turnNeck();
                break;
        }
    }
    public void initview(){

        openmouthButton = (Button) findViewById(R.id.speak);
        stopopenmouthButton = (Button) findViewById(R.id.stopspeak);
        makefaceButton = (Button) findViewById(R.id.makeface);
        makesadButton = (Button) findViewById(R.id.makesadness);
        smileButton = (Button) findViewById(R.id.smile);
        kissButton = (Button) findViewById(R.id.kiss);
        eyesrotateButton = (Button) findViewById(R.id.eyesrotate);
        blinkeyesButton = (Button) findViewById(R.id.blinkeyes);
        blinklefteyesButton = (Button) findViewById(R.id.blinklefteyes);
        blinkrighteyesButton = (Button) findViewById(R.id.blinkrighteyes);
        turnNeckButton = (Button) findViewById(R.id.turnNeck);
        stopopenmouthButton.setOnClickListener(this);
        makefaceButton.setOnClickListener(this);
        makesadButton.setOnClickListener(this);
        openmouthButton.setOnClickListener(this);
        smileButton.setOnClickListener(this);
        kissButton.setOnClickListener(this);
        eyesrotateButton.setOnClickListener(this);
        blinkeyesButton.setOnClickListener(this);
        blinklefteyesButton.setOnClickListener(this);
        blinkrighteyesButton.setOnClickListener(this);
        blinkrighteyesButton.setOnClickListener(this);
        turnNeckButton.setOnClickListener(this);
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {


    }
}
