.gradle
    contains log/caches/outputFiles.bin

.idea
    project layout, includes paths, modules, libraries, caches, ...

app
    general creation of android-apps, important for creating/using buttons

build\intermediate\lint-cache\maven.google\com\android\support\constraint\group-index.xml
    contains android support constraint data in xml-format

build\intermediates\lint-cache\maven.google\com\android\support\test\espresso\group-index.xml
    contains andriod support data for fast testing in xml-format

build\intermediates\lint-cache\maven.google\com\android\support\test\group-index.xml
    contains android support data for testing in xml-format

build\intermediates\lint-cache\maven.google\com\android\support\group-index.xml
    contains andriod support data in xml-format

build\intermediates\lint-cache\maven.google\master-index.xml
    contains android metadata in xml-format

build\intermediates\lint-cache\sdk-registry.xml
    contains sdk libraries + version information in xml-format

gradle\wrapper\gradle-wrapper.jar

gradle\wrapper\gradle-wrapper.properties
    containsgradle wrapper properties

.gitignore

build.gradle
    add configuration options for all sub-projects/modules

gradle.properties
    see notes in file:
    Project-wide Gradle settings.
    IDE (e.g. Android Studio) users:
    Gradle settings configured through the IDE *will override*
    any settings specified in this file.
    For more details on how to configure your build environment visit
    http://www.gradle.org/docs/current/userguide/build_environment.html
    Specifies the JVM arguments used for the daemon process.
    The setting is particularly useful for tweaking memory settings.
    existing code in file: org.gradle.jvmargs=-Xmx1536m
    When configured, Gradle will run in incubating parallel mode.
    This option should only be used with decoupled projects. More details, visit
    http://www.gradle.org/docs/current/userguide/multi_project_builds.html#sec:decoupled_projects
    org.gradle.parallel=true

gradlew
    contains Gradle start up script for UN*X

gradlew.bat
    contains Gradle startup script for Windows

hardwarecontrl.iml
    xml file containing information on module components

hardwarecontrl.jks

local.properties
    see notes in file:
    This file must *NOT* be checked into Version Control Systems,
    as it contains information specific to your local configuration.
    Location of the SDK. This is only used by Gradle.
    For customization when using a Version Control System, please read the
    header note.

    directories for ndk and sdk, should be adjusted for local

settings.gradle
    include ':app'